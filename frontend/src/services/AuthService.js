import axios from "axios";
import authHeader from "./AuthHeader";
import api from "./api";
import TokenService from "./TokenService";

const API_URL = "http://ec2-3-110-151-56.ap-south-1.compute.amazonaws.com:8080/api/auth/";
const getAllUsers = () => {
      return axios.get(API_URL+"users", { headers: authHeader() });
  }
const register = (username, email, password, googleId) => {
    return api.post("/auth/signup", {
        username,
        email,
        password,
        googleId
    });
};

const login = (username, password,googleId) => {
    return api
        .post("/auth/signin", {
            username,
            password,
            googleId
        })
        .then((response) => {
            if (response.data.accessToken) {
                TokenService.setUser(response.data);
            }

            return response.data;
        });
};

const logout = () => {
    TokenService.removeUser();
};

const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem("user"));
};

export default {
    register,
    login,
    logout,
    getCurrentUser,
    getAllUsers
};