import axios from 'axios';
import authHeader from "./AuthHeader";
const TODOLIST_API_BASE_URL = 'http://ec2-3-110-151-56.ap-south-1.compute.amazonaws.com:8080/api/auth/';

class TodoListService{

    getTodoList(){
        return axios.get(TODOLIST_API_BASE_URL, { headers: authHeader() });
    }
    deleteTodo(id){
        return axios.delete(TODOLIST_API_BASE_URL+id,{ headers: authHeader() });
    }
    addTodo({todoText, todoStatus, dueDate, userName}){
        return axios.post(TODOLIST_API_BASE_URL,{todoText,todoStatus,dueDate,userName},{ headers: authHeader() });
    }
    updateTodo(id){
        return axios.get(TODOLIST_API_BASE_URL+id,{ headers: authHeader() });
    }
    editTodo({id,todoText, todoStatus, dueDate, userName}){
        return axios.put(TODOLIST_API_BASE_URL,{id,todoText, todoStatus, dueDate,userName},{ headers: authHeader() });
    }
}

export default new TodoListService();