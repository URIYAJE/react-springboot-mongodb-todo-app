import React from "react";
import AuthService from "../services/AuthService";
import "../styles/Profile.css"

const Profile = () => {
    const currentUser = AuthService.getCurrentUser();

    return (
        <div className="container">
            <div className="padding">
                <div className="row container d-flex justify-content-center">
                    <div className="card user-card-full">
                        <div className="row m-l-0 m-r-0">
                            <div className="col-sm-4 bg-c-lite-green user-profile">
                                <div className="card-block text-center text-white">
                                    <div className="m-b-25">
                                        <img
                                            src="https://img.icons8.com/bubbles/100/000000/user.png"
                                            className="img-radius" alt="User-Profile-Image"/></div>
                                </div>
                            </div>
                            <div className="col-sm-8">
                                <div className="card-block">
                                    <h6 className="m-b-20 p-b-5 b-b-default f-w-600">Personal Information</h6>
                                    <hr/>
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <h6 className="m-b-10 f-w-600">Username</h6>
                                            <p className="text-muted f-w-400">{currentUser.username}</p>
                                        </div>
                                        <div className="col-sm-6">
                                            <h6 className="m-b-10 f-w-600">Email</h6>
                                            <p className="text-muted f-w-400">{currentUser.email}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Profile;