import React, {useState, useEffect} from 'react';
import {RiCloseCircleLine} from "react-icons/ri";
import {TiEdit} from "react-icons/ti";
import TodoListService from '../services/TodoListService';
import "../styles/TodoList.css"
import AuthService from "../services/AuthService";
import EventBus from "../common/EventBus";
import {useNavigate} from "react-router-dom";

function TodoList() {
    const currentUser = AuthService.getCurrentUser();
    const [todolist, setTodolist] = useState([]);
    const [todoText, setTodoText] = useState();
    const [todoStatus, setTodoStatus] = useState();
    const [dueDate, setDueDate] = useState();
    const [id, setId] = useState(0);
    const [userName, setUserName]=useState(currentUser.username);
    const [content, setContent] = useState(true);
    const navigate = useNavigate();
    useEffect(() => {
        if(userName!== null) {
            TodoListService.getTodoList().then(
                (response) => {
                    setTodolist(response.data)
                    console.log(response.data);
                },
                (error) => {
                    setContent(false);

                    if (error.response && error.response.status === 401) {
                      EventBus.dispatch("logout");
                    }
                    window.alert("Session Expired.. Please login again");
                    navigate("/");
                    window.location.reload();
                }
            )
        }
    }, []);

    const getTodoList = () => {
        if(userName!== null) {
            TodoListService.getTodoList().then((response) => {
                setTodolist(response.data)
                console.log(response.data);
            });
        }
    };

    const addTodo = (e) => {
        if (id === 0) {
            TodoListService.addTodo({todoText: todoText, todoStatus: todoStatus, dueDate: dueDate, userName:userName})
                .then(response => {
                    console.log('todo added successfully', response.data);
                    getTodoList();
                })
                .catch(error => {
                    console.log('Something went wrong', error);
                })
        } else {
            TodoListService.editTodo({id: id, todoText: todoText, todoStatus: todoStatus, dueDate: dueDate, userName:userName})
                .then(response => {
                    console.log('todo updated successfully', response.data);
                    getTodoList();
                })
                .catch(error => {
                    console.log('Something went wrong', error);
                })
        }
        setTodoStatus('');
        setDueDate('');
        setTodoText('');
    }
    const updateTodo = (id) => {
        TodoListService.updateTodo(id)
            .then(response => {
                setId(response.data.id);
                setTodoText(response.data.todoText);
                setTodoStatus(response.data.todoStatus);
                setDueDate(response.data.dueDate);
                console.log(response.data);
            })
            .catch(error => {
                console.log('Something went wrong', error);
            })
    }

    const deleteTodo = (id) => {
        TodoListService.deleteTodo(id)
            .then(response => {
                console.log('todo deleted successfully', response.data);
                getTodoList();
            })
            .catch(error => {
                console.log('Something went wrong', error);
            })
    }
    return (
        <div> {content ?  (
        <div>
            <h1>What's Your Plan?</h1>
            <form className="todo-form">
                <div>
                    <div className='todo-input'>
                        <input
                            type='text'
                            placeholder='Add todo'
                            name="todoText"
                            value={todoText}
                            className="todo-text-input"
                            onChange={event => setTodoText(event.target.value)}
                        />
                        <label htmlFor="dueDate">Due Date:</label>
                        <input
                            type='date'
                            placeholder='Select due date'
                            name="dueDate"
                            value={dueDate}
                            onChange={event => setDueDate(event.target.value)}
                        />
                        <label htmlFor="status">Status:</label>

                        <select name="todoStatus" id="status" value={todoStatus}
                                onChange={event => setTodoStatus(event.target.value)}>
                            <option value="default">- Select Status -</option>
                            <option value="Pending">Pending</option>
                            <option value="Complete">Complete</option>
                            <option value="Ongoing">Ongoing</option>
                        </select>
                        <button
                            className='todo-button'
                            type="button"
                            onClick={addTodo}
                        >Submit TODO
                        </button>
                    </div>

                    <div className="todo-body">
                        <div>
                            <h4 className="todo-row-Pending">Pending</h4>
                            {todolist.filter(todolist => todolist.userName===userName).sort((a, b) => a.dueDate > b.dueDate ? 1 : -1).map((todo, index) => (
                                todo.todoStatus === "Pending" ? (
                                    <>
                                        <div key={todo.id} className="todo-row">
                                            <p>{todo.dueDate} | {todo.todoText}</p>
                                            <div className="icons">
                                                <TiEdit
                                                    className='edit-icon'
                                                    type="button"
                                                    onClick={() => {
                                                        updateTodo(todo.id);
                                                    }}
                                                />
                                                <RiCloseCircleLine
                                                    className='delete-icon'
                                                    type="button"
                                                    onClick={() => {
                                                        deleteTodo(todo.id);
                                                    }}/>
                                            </div>
                                        </div>
                                    </>
                                ) : (
                                    <>
                                    </>
                                )
                            ))}
                            <h4 className="todo-row-Ongoing">On Going</h4>
                            {todolist.filter(todolist => todolist.userName===userName).sort((a, b) => a.dueDate > b.dueDate ? 1 : -1).map((todo, index) => (
                                todo.todoStatus === "Ongoing" ? (
                                    <>
                                        <div key={todo.id} className="todo-row">
                                            <p>{todo.dueDate} | {todo.todoText}</p>
                                            <div className="icons">
                                                <TiEdit
                                                    className='edit-icon'
                                                    type="button"
                                                    onClick={() => {
                                                        updateTodo(todo.id);
                                                    }}
                                                />
                                                <RiCloseCircleLine
                                                    className='delete-icon'
                                                    type="button"
                                                    onClick={() => {
                                                        deleteTodo(todo.id);
                                                    }}/>
                                            </div>
                                        </div>
                                    </>
                                ) : (
                                    <>
                                    </>
                                )
                            ))}
                            <h4 className="todo-row-Complete">Complete</h4>
                            {todolist.filter(todolist => todolist.userName===userName).sort((a, b) => a.dueDate > b.dueDate ? 1 : -1).map((todo, index) => (
                                todo.todoStatus === "Complete" ? (
                                    <>
                                        <div key={todo.id} className="todo-row">
                                            <p>{todo.dueDate} | {todo.todoText}</p>
                                            <div className="icons">
                                                <TiEdit
                                                    className='edit-icon'
                                                    type="button"
                                                    onClick={() => {
                                                        updateTodo(todo.id);
                                                    }}
                                                />
                                                <RiCloseCircleLine
                                                    className='delete-icon'
                                                    type="button"
                                                    onClick={() => {
                                                        deleteTodo(todo.id);
                                                    }}/>
                                            </div>
                                        </div>
                                    </>
                                ) : (
                                    <>
                                    </>
                                )
                            ))}
                        </div>
                    </div>

                </div>
            </form>
        </div>
        ):(
             <div className="container">
               <header className="jumbotron">
                 <h3>Session Time out please login again</h3>
               </header>
             </div>
         ) }
        </div>
    )
}

export default TodoList;