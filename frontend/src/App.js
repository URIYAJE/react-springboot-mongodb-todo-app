import React, {useState, useEffect} from "react";
import {Routes, Route, Link} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AuthService from "./services/AuthService";

import Login from "./components/Login";
import Register from "./components/Register";
import Profile from "./components/Profile";
import TodoList from "./components/TodoList";
import TodolistAll from "./components/TodolistAll";
import EventBus from "./common/EventBus";

const App = () => {
    const [showAdminBoard, setShowAdminBoard] = useState(false);
    const [currentUser, setCurrentUser] = useState(undefined);
    const user = AuthService.getCurrentUser();
    useEffect(() => {
        if (user) {
            setCurrentUser(user);
            setShowAdminBoard(user.roles.includes("ROLE_ADMIN"));
        }

        EventBus.on("logout", () => {
          logOut();
        });

        return () => {
          EventBus.remove("logout");
        };
    }, []);

    const logOut = () => {
        AuthService.logout();
        setShowAdminBoard(false);

    };

    return (
        <div>
            <nav className="navbar-expand navbar navbar-dark bg-primary">
                <div className="navbar-brand">
                    MY PLAN
                </div>
                <div className="navbar-nav mr-auto">

                    {currentUser && (
                        <div className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link to={"/profile"} className="nav-link">
                                    {user.username}
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to={"/todolist"} className="nav-link">
                                    My Todo List
                                </Link>
                            </li>
                        </div>
                    )}

                    {showAdminBoard && (
                        <div className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link to={"/todolistall"} className="nav-link">
                                    All todo lists
                                </Link>
                            </li>
                        </div>
                    )}
                </div>

                {currentUser ? (
                    <div className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <a href="/" className="nav-link" onClick={logOut}>
                                LogOut
                            </a>
                        </li>
                    </div>
                ) : (
                    <div className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link to={"/"} className="nav-link">
                                Log In
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to={"/register"} className="nav-link">
                                Sign Up
                            </Link>
                        </li>
                    </div>
                )}
            </nav>
            <div className="container mt-3 p-3">
                <Routes>
                    <Route exact path="/" element={<Login/>}/>
                    <Route exact path="/register" element={<Register/>}/>
                    <Route exact path="/profile" element={<Profile/>}/>
                    <Route exact path="/todolist" element={<TodoList/>}/>
                    <Route exact path="/todolistall" element={<TodolistAll/>}/>
                </Routes>
            </div>

        </div>
    );
};

export default App;