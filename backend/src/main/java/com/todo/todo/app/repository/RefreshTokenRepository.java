package com.todo.todo.app.repository;

import com.todo.todo.app.model.RefreshToken;
import com.todo.todo.app.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RefreshTokenRepository extends MongoRepository<RefreshToken, String> {
    @Override
    Optional<RefreshToken> findById(String id);

    Optional<RefreshToken> findByToken(String token);

    int deleteByUser(User user);
}
