package com.todo.todo.app.repository;

import com.todo.todo.app.model.ERole;
import com.todo.todo.app.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByName(ERole name);
}