package com.todo.todo.app.controller;

import com.todo.todo.app.exception.TokenRefreshException;
import com.todo.todo.app.model.ERole;
import com.todo.todo.app.model.RefreshToken;
import com.todo.todo.app.model.Role;
import com.todo.todo.app.model.User;
import com.todo.todo.app.payload.request.LoginRequest;
import com.todo.todo.app.payload.request.SignupRequest;
import com.todo.todo.app.payload.request.TokenRefreshRequest;
import com.todo.todo.app.payload.response.JwtResponse;
import com.todo.todo.app.payload.response.MessageResponse;
import com.todo.todo.app.payload.response.TokenRefreshResponse;
import com.todo.todo.app.repository.RoleRepository;
import com.todo.todo.app.repository.UserRepository;
import com.todo.todo.app.secuirty.jwt.JwtUtils;
import com.todo.todo.app.service.RefreshTokenService;
import com.todo.todo.app.service.UserDetailsImpl;
import com.todo.todo.app.service.UserDetailsServiceImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    RefreshTokenService refreshTokenService;

    @GetMapping("/users")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Value("${todo.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    @Value("${todo.app.jwtSecret}")
    private String jwtSecret;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        ResponseEntity<?> responseEntity = null;

        if(loginRequest.getPassword() == null) {
            Optional<User> userOptional = userRepository.findByUsername(loginRequest.getUsername());
            if(userOptional.isPresent()) {
                User user = userOptional.get();
                String userId = user.getId();
                String currentUsersGoogleId = user.getGoogleId();

                if (loginRequest.getGoogleId().equals(currentUsersGoogleId)) {
                    String jwt = Jwts.builder()
                            .setSubject((loginRequest.getUsername()))
                            .setIssuedAt(new Date())
                            .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                            .signWith(SignatureAlgorithm.HS512, jwtSecret)
                            .compact();
                    RefreshToken refreshToken = refreshTokenService.createRefreshToken(userId);
                    List<String> roles = convertSetToList(user.getRoles());

                    responseEntity = ResponseEntity.ok(new JwtResponse(jwt,
                            refreshToken.getToken(),
                            userId,
                            loginRequest.getUsername(),
                            user.getEmail(),
                            roles));
                } else {
                    System.out.println(
                            "Credentials don't match"
                    );
                    ResponseEntity
                            .badRequest()
                            .body(new MessageResponse("Error: Credentials don't match"));
                }
            } else {
                System.out.println("User is not available in the database");
                ResponseEntity
                        .badRequest()
                        .body(new MessageResponse("Error: User is not available in the database"));
            }
        } else if(loginRequest.getGoogleId() == null) {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
            List<String> roles = userDetails.getAuthorities().stream()
                    .map(item -> item.getAuthority())
                    .collect(Collectors.toList());
            RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());

            responseEntity = ResponseEntity.ok(new JwtResponse(jwt,
                    refreshToken.getToken(),
                    userDetails.getId(),
                    userDetails.getUsername(),
                    userDetails.getEmail(),
                    roles));
        }
        return responseEntity;


    }

    public static List<String> convertSetToList(Set<Role> set)    {
        List<String> list =new ArrayList<>();
         for (Role t : set){
             list.add(String.valueOf(t.getName()));
         }
         return list;
    }

    @PostMapping("/refreshtoken")
    public ResponseEntity<?> refreshtoken(@Valid @RequestBody TokenRefreshRequest request) {
        String requestRefreshToken = request.getRefreshToken();

        return refreshTokenService.findByToken(requestRefreshToken)
            .map(refreshTokenService::verifyExpiration)
            .map(RefreshToken::getUser)
            .map(user -> {
                String token = jwtUtils.generateTokenFromUsername(user.getUsername());
                return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken));
            })
            .orElseThrow(() -> new TokenRefreshException(requestRefreshToken,
                "Refresh token is not in database!"));
    }


    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        if(signUpRequest.getPassword() == null){
            User user = new User(signUpRequest.getUsername(),
                    signUpRequest.getEmail(),
                    signUpRequest.getPassword(),
                    signUpRequest.getGoogleId());

            Set<String> strRoles = signUpRequest.getRoles();
            Set<Role> roles = new HashSet<>();

            if (strRoles == null) {
                Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
            } else {
                strRoles.forEach(role -> {
                    switch (role) {
                        case "admin":
                            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(adminRole);

                            break;
                        default:
                            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(userRole);
                    }
                });
            }

            user.setRoles(roles);
            userRepository.save(user);
        }else if(signUpRequest.getGoogleId() == null){
            User user = new User(signUpRequest.getUsername(),
                    signUpRequest.getEmail(),
                    encoder.encode(signUpRequest.getPassword()),
                    signUpRequest.getGoogleId());

            Set<String> strRoles = signUpRequest.getRoles();
            Set<Role> roles = new HashSet<>();

            if (strRoles == null) {
                Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
            } else {
                strRoles.forEach(role -> {
                    switch (role) {
                        case "admin":
                            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(adminRole);

                            break;
                        default:
                            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(userRole);
                    }
                });
            }

            user.setRoles(roles);
            userRepository.save(user);
        }
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
}
